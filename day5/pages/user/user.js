const app = getApp()
//console.log(app)
Page({
  data: {
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),//判断小程序的API，回调，参数，组件等是否在当前版本可用。
    total: 0,
    cartList: [] // 购物车列表
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({//保留当前页面，跳转到应用内的某个页面。
      url: '../logs/logs'
    })
  },
  onLoad: function() {
    let ary = app.globalData.cartList;//获取选中课程列表的数组
    let t = ary.reduce((prev, next) => prev + next.price * next.count, 0)//课程的总价 0是初始值
    //console.log(app.globalData, ary, t)
    this.setData({
      cartList: ary,
      total: t
    })
    if (app.globalData.userInfo) {//获得用户的信息就并且有权限
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
  getUserInfo: function(e) {
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  change(e) {
    let ary = app.globalData.cartList;
    ary.forEach(item => {//获取表框的数字
      item.id == e.target.dataset.item.id ? item.count = e.detail.value * 1 : null
    })
    let t = ary.reduce((prev, next) => prev + next.price * next.count, 0)//获得表框数字的课程的总价
    //console.log(e, ary, t)
    this.setData({
      cartList: ary,
      total: t
    })
  },
  onShow() {
    let ary = app.globalData.cartList;
    let t = ary.reduce((prev, next) => prev + next.price * next.count, 0);
    //console.log(ary, t)
    this.setData({
      cartList: ary,
      total: t
    })
  }
})