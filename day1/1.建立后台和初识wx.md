# 后台数据
```git
npm install express express-session -S
```
<a name="fbA43"></a>
### server文件目录
```git
├─promiseFs.js//读写文件
├─server.js//端口文件
├─json//数据
|  ├─banner.json//轮播图
|  ├─classList.json//课程
|  └publicClassList.json//四张图和描述
```
server/json/banner.json文件
```json
{
    "list":[
      "http://www.zhufengpeixun.cn/main/img/banner01.png",
      "http://www.zhufengpeixun.cn/main/img/banner02.png",
      "http://www.zhufengpeixun.cn/main/img/banner03.png"
    ]
  }
```
server/json/classList.json文件
```json
{
    "level1":[
      {
        "img":"http://www.zhufengpeixun.cn/videoCourse/images/video_html.png",
        "title":"珠峰HTML+CSS精讲视频",
        "price":10,
        "count":156,
        "id":1
      },
      {
        "img":"http://www.zhufengpeixun.cn/videoCourse/images/video_h5.png",
        "title":"HTML5+CSS3精讲视频",
        "price":20,
        "count":156,
        "id":2
      },
      {
        "img":"http://www.zhufengpeixun.cn/videoCourse/images/html5.jpg",
        "title":"HTML5实战 - webApp开发视频",
        "price":30,
        "count":156,
        "id":3
      },
      {
        "img":"http://www.zhufengpeixun.cn/videoCourse/images/video_js.png",
        "title":"JavaScript基础视频",
        "price":40,
        "count":156,
        "id":4
      }
    ],
    "level2":[
      {
        "img":"http://pic3.ablesky.cn/content/pic/coursephoto/2017/12/08/643a5e6e-58a4-41b4-bfd0-b9d2cd87ac3d.jpg",
        "title":"2017年前端全栈工程化开发视频",
        "price":20,
        "count":156,
        "id":5
      },
      {
        "img":"http://www.zhufengpeixun.cn/videoCourse/images/es6.jpg",
        "title":"珠峰名师带你玩转ES6",
        "price":50,
        "count":156,
        "id":6
      },
      {
        "img":"http://www.zhufengpeixun.cn/videoCourse/images/jquery.jpg",
        "title":"JQUERY源码解读和实战应用",
        "price":60,
        "count":156,
        "id":7
      },
      {
        "img":"http://www.zhufengpeixun.cn/videoCourse/images/ajax.jpg",
        "title":"珠峰Ajax原理系列视频",
        "price":990,
        "count":156,
        "id":8
      },
      {
        "img":"http://www.zhufengpeixun.cn/videoCourse/images/js.jpg",
        "title":"珠峰web前端-闭包精讲视频",
        "price":660,
        "count":156,
        "id":9
      },
      {
        "img":"http://www.zhufengpeixun.cn/videoCourse/images/js_dx.jpg",
        "title":"珠峰web前端-面向对象视频",
        "price":770,
        "count":156,
        "id":10
      },
      {
        "img":"http://www.zhufengpeixun.cn/videoCourse/images/reg.jpg",
        "title":"珠峰web前端-正则实战视频",
        "price":220,
        "count":156,
        "id":11
      }
    ],
    "level3":[
      {
        "img":"http://www.zhufengpeixun.cn/videoCourse/images/video_node.png",
        "title":"Node.js基础入门视频",
        "price":220,
        "count":156,
        "id":12
      },
      {
        "img":"http://www.zhufengpeixun.cn/videoCourse/images/new5.png",
        "title":"Vue从理论到项目实战精讲视频",
        "price":230,
        "count":156,
        "id":13
      },
      {
        "img":"http://www.zhufengpeixun.cn/videoCourse/images/new1.png",
        "title":"微信小程序专家级课程",
        "price":320,
        "count":156,
        "id":14
      },
      {
        "img":"http://www.zhufengpeixun.cn/videoCourse/images/new6.png",
        "title":"React架构视频课程",
        "price":210,
        "count":156,
        "id":15
      }
    ]
  }
```
server/json/publicClassList.json文件
```json
[
    {
      "img":"http://www.zhufengpeixun.cn/public/imgs/vuerouter.png",
      "text":"手写vue路由"
    },
    {
      "img":"http://www.zhufengpeixun.cn/skin/20142/img/g4.jpg",
      "text":"九种跨域方式全揭秘,再也不担心跨域问题了"
    },
    {
      "img":"http://www.zhufengpeixun.cn/skin/20142/img/g3.jpg",
      "text":"MVVM原理剖析:数据劫持，观察者模式,模版编译 "
    },
    {
      "img":"http://www.zhufengpeixun.cn/skin/20142/img/g2.jpg",
      "text":"实现React虚拟DOM和DOM Diff"
    }
  ]
```
server/promiseFs.js文件
```javascript
let fs = require('fs');
let obj = {};
//读文件
['readFile','readdir'].forEach(item=>{
    obj[item] = function (url,encoding=null){
        if(/\.(js|css|html|txt|md)$/.test(url)){
            encoding = 'utf-8'
        }
        return new Promise((res,rej)=>{
            fs[item](url,encoding,(err,data)=>{
                if(!err){
                    res(data)
                }else{
                    rej(err)
                }
            })
        })
    }
});
['mkdir','rmdir','unlink'].forEach(item=>{
    obj[item] = function(url){
        return new Promise((res,rej)=>{
            fs[item](url,(err)=>{
                if(!err){
                    res()
                }else{
                    rej(err)
                }
            })
        })
    }
});
//写文件
['writeFile','appendFile'].forEach(item=>{
    obj[item] = function(url,data,encoding=null){
        if(/\.(js|css|html|txt|md)$/.test(url)){
            encoding = 'utf-8'
        }
        return new Promise((res,rej)=>{
            fs[item](url,data,encoding,(err)=>{
                if(!err){
                    res()
                }else{
                    rej(err)
                }
            })
        })
    }
})
//复制文件
obj.copyFile = function(oldUrl,newUrl){
    return new Promise((res,rej)=>{
        fs.copyFile(oldUrl,newUrl,(err)=>{
            if(!err){
                res()
            }else{
                rej(err)
            }
        })
    })
}
module.exports = obj;
```
server/server.js文件
```javascript
// 实现一个登录的功能
let express = require('express');
let qs = require('qs');
let session = require('express-session');//session保存在服务器上
let { readFile, writeFile } = require('./promiseFs');
let app = express();
//监听端口
app.listen(3000, function () {
  console.log("后端接口服务 起于 3000")
})

// 解决跨域
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', 'http://localhost:3000')
  res.header('Access-Control-Allow-Credentials', true)
  res.header('Access-Control-Allow-Headers', "Content-Type,X-Agent,X-Token,X-Legacy-Token,X-Legacy-Uid,X-Legacy-Device-Id,X-Legacy-New-Token,X-Request-Id")
  req.method == 'OPTIONS' ? res.send('OK') : next();
})

// 把post请求的参数 转成普通对象 存放到req.body上
app.use((req, res, next) => {
  let str = '';
  req.on('data', (chunk) => {
    str += chunk;
  })
  req.on('end', () => {
    let obj = {};
    try {
      obj = JSON.parse(str)
    } catch (error) {
      obj = qs.parse(str)
    }
    req.body = obj;
    next();
  })
})


// 把读取数据的操作放到中间件处理
function setData(url, key, req, res, next) {
  readFile(url).then(data => {
    req[key] = JSON.parse(data);
    next()
  }).catch(err => {
    // 读取失败 给前端500
    res.status(505);
    res.send('505');
  })
}


//轮播图
app.use((req, res, next) => {
  setData('./json/banner.json', 'banner', req, res, next)
})
app.use(session({
  //在这个中间件之后 会在 req上多了一个 session的属性
  name: 'qqq', // 默认  connect.sid
  secret: 'myqqq', // session会根据 这个属性 和后端种在session的属性名 来生成对应的字段
  saveUninitialized: false,//无论有没有session cookie，每次请求都设置个session cookie  默认标识为 connect.sid
  resave: false,//是否每次请求都重新设置session cookie
  cookie: {
    maxAge: 1000 * 60 * 60 * 24 * 30,//cookie从创建到过期所能存在的时间，以秒为单位
  }
}));
app.get('/banner', (req, res) => {//发送的数据
  res.send({
    code: 0,
    data: req.banner.list
  })
})

//首页四张图和文字描述
app.use((req, res, next) => {
  setData('./json/publicClassList.json', 'publicList', req, res, next)
})
app.get('/publicList', (req,res) => {
  res.send({
    code: 0,
    data: req.publicList
  })
})


//多个课程数据
app.use((req, res, next) => {
  setData('./json/classList.json', 'classList', req, res, next)
})
app.get('/classList', (req,res) => {
  res.send({
    code: 0,
    data: req.classList
  })
})
```
在控制台上输出: node .\server.js 回车后出现：后端接口服务 起于 3000   即可开启成功！
<a name="UQ4mx"></a>
### 微信初识
文档查看：[https://developers.weixin.qq.com/miniprogram/dev/framework/quickstart/getstart.html#%E7%94%B3%E8%AF%B7%E5%B8%90%E5%8F%B7](https://developers.weixin.qq.com/miniprogram/dev/framework/quickstart/getstart.html#%E7%94%B3%E8%AF%B7%E5%B8%90%E5%8F%B7)<br />小程序注册的网址：[https://mp.weixin.qq.com/wxopen/waregister?action=step1](https://mp.weixin.qq.com/wxopen/waregister?action=step1)<br />登录小程序后台的网址，我们可以在菜单 “开发”-“开发设置” 看到小程序的 **AppID** 了 。<br />小程序的 AppID 相当于小程序平台的一个身份证，后面会用到**AppID** 。<br />安装开发工具：[https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html](https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html)<br />下载的版本是：稳定版 [Stable Build](https://developers.weixin.qq.com/miniprogram/dev/devtools/stable.html) (1.03.2006090)<br />**第一个小程序：**<br />新建项目选择小程序项目，选择代码存放的硬盘路径，填入刚刚申请到的小程序的 AppID，给你的项目起一个好听的名字，勾选 "不使用云服务" 。【注意: 你要选择一个空的目录才可以创建项目】，点击新建，你就得到了你的第一个小程序了，点击顶部菜单编译就可以在微信开发者工具中预览你的第一个小程序。<br />我的文件名称是app文件夹。
<a name="ZGF1Y"></a>
### 小程序代码构成

- `.json` 后缀的 `JSON` 配置文件
- `.wxml` 后缀的 `WXML` 模板文件
- `.wxss` 后缀的 `WXSS` 样式文件
- `.js` 后缀的 `JS` 脚本逻辑文件
<a name="G7Eip"></a>
#### json配置
JSON 是一种数据格式，并不是编程语言，在小程序中，JSON扮演的静态配置的角色。<br />`app.json` 是当前小程序的全局配置，包括了小程序的所有页面路径、界面表现、网络超时时间、底部 tab 等。<br />这个配置各个项的含义:

- **pages字段 **—— 用于描述当前小程序所有页面路径，这是为了让微信客户端知道当前你的小程序页面定义在哪个目录
- **window字段 **—— 定义小程序所有页面的顶部背景颜色，文字颜色定义等
<a name="YZWP7"></a>
#### wxml模板
`WXML` 充当的就是类似 `HTML` 的角色，小程序的 `WXML` 用的标签是 `view`, `button`, `text` 等等，这些标签就是小程序给开发者包装好的基本能力，还提供了地图、视频、音频等等组件能力。还多了一些 `wx:if` 这样的属性以及 {{ }} 这样的表达式。
<a name="FqA0M"></a>
#### wxss样式
`WXSS` 具有 `CSS` 大部分的特性，小程序在 `WXSS` 也做了一些扩充和修改。<br />新增了尺寸单位。在写 `CSS` 样式时，开发者需要考虑到手机设备的屏幕会有不同的宽度和设备像素比，采用一些技巧来换算一些像素单位。`WXSS` 在底层支持新的尺寸单位 `rpx` ，开发者可以免去换算的烦恼，只要交给小程序底层来换算即可，由于换算采用的浮点数运算，所以运算结果会和预期结果有一点点偏差。<br />提供了全局的样式和局部样式。和前边 `app.json`, `page.json` 的概念相同，你可以写一个 `app.wxss` 作为全局样式，会作用于当前小程序的所有页面，局部页面样式 `page.wxss` 仅对当前页面生效。
<a name="7EnJ1"></a>
#### js逻辑交互
在 JS 中调用小程序提供的丰富的 API，利用这些 API 可以很方便的调起微信提供的能力，例如获取用户信息、本地存储、微信支付等。<br />温馨提示：有时间的可以看看小程序文档，会对接下来的敲代码是很有帮助的。
