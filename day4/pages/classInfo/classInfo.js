// pages/classInfo/classInfo.js
let app = getApp();
// console.log(app);//cartList
Page({
  /**
   * 页面的初始数据
   */
  data: {
    til: '',
    img: '',
    id:'',
    price:0,
    num:app.globalData.cartList.length
  },
  add() {
    let ary = app.globalData.cartList
    let obj = {
      title: this.data.til,
      img: this.data.img,
      id: this.data.id,
      price:this.data.price,
      count:1
    }
    let bol=ary.filter(item=>item.id == this.data.id);//选中的课程
    if(bol.length){//如果有的话，数量增加
      bol[0].count++
    }else{//没有的话就新增 课程种类的数量增加，而不是点击的数量
      ary.push(obj);
      this.setData({
        num:ary.length
      })
    }
  },
  goUser() {
    wx.switchTab({//需要跳转的 tabBar 页面的路径 (代码包路径)，路径后不能带参数。
      url: '/pages/user/user'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // console.log(options)
    this.setData({
      til: options.til,
      img: options.img,
      id:options.id,
      price:options.price,
    })
    this.setData({
      num:app.globalData.cartList.length
    })
  },
})     