import http from '../../utils/http.js'
//index.js
//获取应用实例
const app = getApp()
//console.log(app)
Page({
  data: {
    bannerList: [],//轮播图存值
    classList:[],//四张图存值
    showKnow:false,//默认'认识珠峰'板块的video是关闭的
    showzf:false//默认'珠峰故事'板块的video是关闭的
  },

  onLoad: function () {//当页面载入完成后加载js部分
    this.getBanner();//轮播图
    this.getPublicClass();//四张图
  },
  getBanner(){
    http.get('/banner').then(data=>{//获取轮播图接口
      if(data.code == 0){
        this.setData({
          bannerList:data.data//返回数据
        })
      }
    })
  },
  getPublicClass(){
    http.get('/publicList').then(data=>{//获取四张图接口
      if(data.code==0){
        this.setData({
          classList:data.data//返回数据
        })
      }
    })
  },
  closeKnow(e){
    this.setData({
      showKnow:e.detail//点击按钮时触发获得的参数
    })
  },
  showKonw(){//显示出video
    this.setData({
      showKnow: true
    })
  },
  closezf(e) {
    this.setData({
      showzf: e.detail
    })
  },
  showzf() {
    this.setData({
      showzf: true
    })
  }
})